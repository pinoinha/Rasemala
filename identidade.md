# Sobre identidade

Atualmente estou numa crise de identidade. E não uma pequena ou momentânea, mas uma que tem se estendido há muito tempo e me feito sofrer.

Graças à minha companheira consegui perceber isso, e pela primeira vez na vida consegui expressar toda essa minha dor de existir pra alguém, com palavras, exemplos, aforismas. Foi um fluxo intenso de informação e emoção, e foi muito difícil pra mim. Mas, no final, fiquei aliviado. Aliviado por ter uma pessoa na qual posso confiar, com quem compartilhar, refletir, ouvir. Que sorte a minha!

Mas como essas crises são difíceis, não? Especialmente quando, como aconteceu comigo, são silenciosas; perenes; duradouras. Eu devo estar assim há anos, e eu não estou brincando. Eu só não conseguia ver. 

Ver. Visão. Clareza. Clareza de quem eu sou? Não, não tenho como ter. Eu sou um ser limitado, e só me enxergo por dentro. Não tenho ideia de como sou por fora, embora tenha uma mínima noção por conta das pessoas ao meu redor. Mas ainda assim, nunca terei a visão completa. Não sou e não serei capaz de ver e entender essa criatura que eu sou, por inteiro. Só partes.

Felizmente, o tempo é grande aliado nisso tudo. Eu disse "é"? Bem, pode ser. Se você souber usá-lo, é claro. O tempo pode ser a grande pá que você usa pra cavar sua cova nessas situações, ou pode ser o martelo que te ajudar a edificar uma torre pra cima. Caso você não reflita sobre si mesmo e suas ações, não busque conhecimento (perdão...) e filosofia, você tem uma pá. Faça isso e, por Deus, você ainda pode ter uma pá. Mas com paciência e vontade, você pode ter um martelo em suas mãos.

Isso soa vago, não? É porque é. Metafísica, afinal! Esse é um texto pessoal de uma pessoa que só conhece de verdade a si própria (será mesmo?), então infelizmente não sei se ajuda caso você esteja também numa crise atualmente. Eu sei que, ao longo dos anos, eu lentamente perseverei. E agora conheço um pouco mais de mim. Não o suficiente, nunca o suficiente. Mas o pouco que conheço, valorizo.

Mas o nome desse texto é identidade, e embora eu esteja falando sobre esse tema de forma indireta vou falar agora do jeito mais direto que eu consigo:

**Eu não sei quem eu sou.**

Eu não sei qual o meu futuro, eu conheço meu passado e vivo meu presente. Mas eu não me encontro onde quer que procure. Eu sei meu nome, é claro! Sei do que eu gosto, do que eu não gosto, quem eu amo e quem odeio. Conheço meus hobbies e prazeres, e minhas dores. Mas eu nunca vou conhecer completamente essa criatura que eu chamo de "eu".

E não há beleza nisso? Embora uma beleza melancólica. Não saber quem se é talvez seja um dos maiores sofrimentos filosóficos que existam. Mas, não é belo? Ser um ser tão incrivelmente complexo, amplo, diverso e poderoso (sim, poderoso!) que não pode ser completamente mapeado? Isso não traz fascínio e curiosidade a seus olhos?

Eu tenho sorte de, entre tantas características, ter uma queda por essas coisas. Pela metafísica do ser, pela suposta "essência" que nos torna quem somos. Isso faz com que, mesmo em momentos de sofrimento existencial, eu ainda fique fascinado, motivado, curioso; embora dolorido. Sou um egoísta, afinal!

E que isso fique anotado, de vez por todas: eu não sei e nunca saberei quem sou por completo. Deixa-me gritar isso a plenos pulmões nas montanhas do Quirguistão, se for necessário. Mas, ao fim de meu breve fôlego, também deixa-me gritar que eu amo estar vivo. Que minha existência é a melhor coisa que já tive, e que quero vivê-la ao máximo. 

A crise perdura, e persevera. Mas ela acaba. A crise sempre passará, e ao final eu vou estar lá. Dolorido, em recuperação, sem dúvidas; mas indubitavelmente mais forte. Se não, mais sábio (por que não os dois?).

Ó mundo, ó existência, ó dor! Me tragas tudo o que tens a oferecer. Preparado não estou, nunca estarei. Mas eu, por tudo, passarei.
Ao final da chuva, estarei de pé e ensopado, mas com um chá quente na mão.

Seria esse o mistério da vida?
