# Castlevania: tragédia e gentileza

Ultimamente eu tenho tido um pouco mais de inspiração pra escrever. Isso por alguns motivos, e a maioria deles introspectivos. 

Eu tenho assistido bastante Castlevania, animação (ou anime?) da Netflix. É, sem dúvidas, uma das melhores animações que eu já vi em toda a minha vida. A história cresce, os personagens crescem e você cresce junto. Me lembra histórias como Fullmetal Alchesmist, no sentido que as coisas lentamente escalam e começamos a compreender os grandes esquemas, maquinações e complexidade de tudo que acontece perante nossos olhos.

Os personagens são incríveis, você consegue se relacionar, ver o mundo pela ótica deles e entender o que os leva a fazer o que fazem. Nenhum deles é inteiramente bom, mas alguns tentam e por vezes conseguem. Outros falham terrivelmente. Mas, no final, eles são humanos. Pessoas que erram e são vítimas, embora eles mesmos façam vítimas.

Não me entenda mal, não endosso nenhuma das ações imorais que os vampiros, humanos e monstros fazem, pelo contrário! Eu acho fascinante como vários dos personagens retratam pessoas que sofreram muito, estão cheias de traumas, dores e tristeza e isso acaba sendo a grande faísca pra que façam as coisas horríveis que fazem. Foram machucadas, e portanto machucam outras. Muitas outras.

E não é assim que funciona no mundo real? Quantas vezes nós não magoamos e ferimos quem amamos porque estávamos num momento difícil? Claro, eu não sei você mas eu nunca fui violento, nunca causei uma fração do sofrimento que esses personagens causam. Mas você consegue entender, minimamente. São pessoas que reagem de maneira extremamente exagerada por conta do medo, do trauma, do sofrimento que passaram. Figuras extremamente tristes, trágicas, mórbidas... 

... e mesmo assim você quer saber mais delas. O quão longe elas irão, até que ponto vão chegar, quanto sofrimento vão causar aos outros e a si mesmos? É quase uma curiosidade mórbida. As primeiras duas temporadas são uma lenta decadência, cheia de tristeza, raiva, mágoa. Uma espiral de melancolia que leva você fundo. Pra muito abaixo da terra.

Apesar disso, vem a terceira temporada. Personagens bons começam a se tornar cinzentos. O mundo mostra suas verdadeiras presas e do que as pessoas são realmente capazes. Guiadas por ambição. Por medo. Por traumas. Os mesmos temas que foram apresentados nas primeiras duas temporadas, veja só. Mas mais profundos, complexos, gritantes.

Dito tudo isso, uma série puramente depressiva e melancólica precisaria ser muito, e eu repito, MUITO boa pra me convencer a continua assistindo. Eu tenho um limite pra dor, pra sofrimento, pra tristeza. E ele não é tão alto assim. Mas, pra minha surpresa, Castlevania desenvolve tudo isso muito bem! Os personagens, os temas, o propósito... tudo é conduzido como uma orquestra. E faz mais! No meio de tantos temas pesados, difíceis, por vezes intragáveis, ele entrega coisas novas: gentileza. Solidão. Confiança. Amor. Amizade.

E você se pega não mais querendo saber só os limites mórbidos, inumanos que as coisas podem alcançar. Não. Você começa a torcer pelos personagens, pra que se tornem pessoas melhores, pra que atinjam seus objetivos, pra que se encontrem. E, não sei se ocorre com a maioria das pessoas que assistem, mas eu me identifiquei mais do que nunca com alguns personagens. Eu me senti representado, vi clareza. Clareza que nem sempre veio pra bem, afinal esse início de semana também tem sido palco pra uma terrível crise existencial (quem sabe um outro texto?).

E, no final, nós retornamos àquilo que esses personagens são, o que representam: seres humanos. Pessoas. Pessoas que, no fundo, são como eu e você. Pessoas que choram, que sofrem, que querem algo melhor pra si mesmas.

Mas também pessoas que se autodestroem. Que ficam presas em seus traumas e se isolam do mundo; que esquecem quem são e cometem atos de desespero contra os outros em busca de sua identidade. Ou, ao menos, o que acham que é sua identidade. Pessoas trágicas, afinal. Algumas se reerguem, outras fica, no chão; outras ainda cavam uma cova mais funda.

Enfim. Castlevania é uma obra com personagens complexos e temas complexos, e eu recomendo fortemente que você assista. Pode começar sem compromisso, com ceticismo e suspeita. Afinal, esse texto é extremamente pessoal! Mas não assista de cara se tiver um problema com gore, morbidez, tristeza, e outros tantos assuntos que abordei. 
