# Como obter feeds RSS?  

---

No meu último texto comentei sobre minha paixão por feeds RSS, e expliquei alguns dos motivos por trás disso.  Pra complementar o assunto, vou demonstrar algumas maneiras de extrair feeds RSS de sites populares, como Reddit e YouTube, mas também de blogs e outras fontes.  

# Reddit  

Talvez o mais simples dessa lista! Se quiser o feed rss de algum subreddit qualquer que seja de seu interesse, basta seguir a fórmula:  

`https://reddit.com/r/<nome_do_subreddit/<seção>.rss`  

Assim, se eu quiser o feed dos posts novos (seção `new`) do subreddit `suckless`, eu uso a seguinte URL:  

`https://reddit.com/r/suckless/new.rss`  

E pronto! Meu navegador me direciona pra uma página/pede pra que eu baixe um arquivo .rss contendo o feed em si. Com essa URL em mãos, basta usar algum cliente pra fazer o processamento e download do conteúdo.

![Newsboat com o feed do subreddit Suckless selecionado](https://codeberg.org/pinoinha/Rasemala/raw/commit/b0181cb49817dba8d829c68297e587aa1fb26168/RSS_como/suckless_newsboat.png) 

# YouTube  

A URL geral para os feeds segue o seguinte formato:  

`https://www.youtube.com/feeds/videos.xml?channel_id=<id_do_canal>`

Assim, pra pegar o feed dos canais primeiro precisamos saber o ID deles. No entanto, o YouTube esconde essa informação para muitos canai, por exemplo o que ocorre com o canal do Brodie Robertson:

`https://www.youtube.com/user/OmegaDungeon`

Isso nos dá o nome de usuário do canal, mas não seu ID. Vejamos como conseguí-lo!  

## Comment Picker

Existem alguns sites que fazem esse trabalho por você, como o [Comment Picker](https://commentpicker.com/youtube-channel-id.php).  

![Comment Picker, na aba de busca de ID de canais do YouTube](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/comment_picker.png)  

Assim, pra obtermos o ID do canal basta inserir o endereço na seção abaixo:  

![Inserindo a URL no Comment Picker](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/inserindo_url.png)

Feito isso, conseguimos várias informações acerca do canal, como sua data de início, descrição, número de inscritos, de visualizações, etc. que fogem do escopo desse texto.  

![Informações acerca do canal](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/brodie_id.png)

E agora temos o ID do canal :)  

## Invidious

Mas a solução que eu uso, prefiro e recomendo é você acessar o canal através do [Invidious](https://github.com/iv-org/invidious). Caso você não conheça, é um front-end alternativo ao YouTube, sem rastreadores e perfeitamente utilizável como cliente principal para a plataforma.  

Essa de fato é a solução mais simples e direta, que pode ser resumida nos seguintes passos:  
1 - Entre na sua instância preferida do Invidious  
2 - Procure o canal pela barra de pesquisa  
3 - Acesse o canal e copie o ID da própria barra de pesquisa  

Executando os passos nas imagens abaixo:

![Página inicial do Invidious](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/invidious.png)

![Procurando o canal](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/pesquisa.png)

![Acessando o canal](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/canal.png)

![Copiando o ID da barra de pesquisa](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/id.png)

Simples, não? Por isso considero a melhor solução.  

De qualquer maneira, o resultado final será o mesmo: iremos preencher a URL "modelo" com o ID e teremos finalmente nosso feed.  

`https://www.youtube.com/feeds/videos.xml?channel_id=UC2eYFnH61tmytImy1mTYvhA`

## Adendo  

Embora eu tenha achado que seria mais interessante mostrar essas maneiras de obter o feed RSS de canais no YouTube, pra pra uso prático tem uma maneira muito mais fácil.  

O Invidious, além de tudo que já faz, ainda te dá o feed do canal em um clique! Na captura de tela do canal do Brodie pode reparar que o famoso símbolo está no canto direito da moldura, alinhado com o nome do canal:  

![Ícone do RSS no Invidious](https://codeberg.org/pinoinha/Rasemala/raw/commit/e5e268d68128d58ad6c9793b3bdb2928255d34b6/RSS_como/rss_invidious.png)

# Blogs, podcasts, etc.  

Eu comentei que conseguir o feed do reddit era talvez o mais simples de se fazer, e é verdade. Eu só não disse que é **com certeza** o mais simples porque quando falamos sobre blogs e podcasts a história é diferente.  

No geral a maioria dos blogs e podcasts já dão fácil acesso aos feeds, felizmente. Basta procurar o famigerado ícone do RSS, como vimos agora no Invidious.  

Por exemplo, no [site do Gugacast](https://gugacast.com/) eles já fornecem o link pra quem quiser:  

`https://feeds.soundcloud.com/users/soundcloud:users:33700067/sounds.rss`

Note que é cortesia do [Soundcloud](https://soundcloud.com/). 

Qualquer podcast que for hospedado no Anchor também dá o feed RSS logo na página do programa, pegando como exemplo o [podcast da Laurinha Lero](https://anchor.fm/laurinhalero):  

![Podcast da Laurinha Lero no Anchor](https://codeberg.org/pinoinha/Rasemala/raw/commit/bb5144597eb458b1607576a29cc124f44916ead9/RSS_como/anchor_rss.png)  
# Conclusão  

A lista ficaria ainda mais exaustiva se eu continuasse dando exemplos. Esse tanto de imagens já vai deixar esse post um pouco mais pesado que a média :)  

Espero que o texto tenha sido de alguma ajuda! Nem sempre é fácil achar esse tipo de material sem ser em inglês.  

Feeds RSS são seu direito de consumo!  
