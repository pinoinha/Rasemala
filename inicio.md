# Rasemala

---

## O que é?  

Eu não faço ideia do motivo de eu ter criado esse blog. Eu só gosto de blogs e das pessoas que os escrevem. Acho que é uma maneira de eu escrever algo que eu gostaria de ler.  

## O que você deveria esperar deste blog?  

Sinceramente, nada. Não tenho disciplina e nem assunto o suficiente pra escrever num ritmo constante. Mas pretendo postar sobre as coisas que eu gosto, tais quais, mas não limitadas a:  
* GNU/Linux
* *BSDs
* Programação
* Universidade pública
* Política (majoritariamente de esquerda)
* Música
* Demais interesses e pensamentos que me vierem à telha

## Como eu posso acompanhar esse blog?  

Assim, eu não sei por que você faria isso, mas o [Bear Blog](https://bearblog.dev/) cria um feed RSS automaticamente, pra minha alegria. Você pode acessá-lo por [aqui](https://pinoinha.bearblog.dev/feed/).

## Quem é você?  

Eu poderia ser muito cringe e voltar aos meus 12 anos, colocando uma descrição edgy de mim mesmo. Mas eu sou só um jovem acima do peso brincando com computadores e tentando ser uma pessoa melhor. Não sei se tenho algo de muito destaque, mas as pessoas dizem que eu sorrio bastante.  

Se quiser, tenho um [perfil no Mastodon](http://mastodon.com.br/@Pinoinha), que basicamente é esse blog mas 24h/dia 7 dias/semana. 

# Código

Absolutamente todos os recursos deste blog (textos e imagens) estão hospedados no meu [Codeberg](https://codeberg.org/pinoinha).

# Licenciamento

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Este obra está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Atribuição-NãoComercial 4.0 Internacional</a>.
