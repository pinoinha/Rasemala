# Hora de Codar!  

Recentemente, no Centro Acadêmico de que eu participo, começamos finalmente um projeto de longa data: ensino de programação pra crianças e jovens de ensino fundamental e médio! Queria compartilhar um pouco sobre como tem sido a experiência.  

# Expectativas

Eu estava extremamente apreensivo com como seria o projeto. Tivemos um pico de inscrições nos três dias anteriores ao início das aulas, mas antes disso o desespero batia muito forte porque tínhamos menos de 10 alunos inscritos e cerca de 30 professores e monitores.  

Imagina só! Meses de preparação, conversando com professores sobre a ementa, batendo datas, organizando material, etc. pra no final ter falta de aluno! Tivemos MUITA sorte que de última hora uma entidade nos contatou e aí sim tivemos o mencionado pico. Eu sinceramente não sei o que faríamos sem eles, são os verdadeiros salvadores desse projeto.  

De qualquer maneira, minha apreensão não acabou por aí. Beleza, conseguimos alunos, mas e se os professores forem **ruins**? Nós não fizemos processo seletivo, porque é beneficente e já tem pré-requisitos que a maioria das pessoas não se arriscaria a contrariar, como... bem... sabe programar em primeiro lugar.  

Mas esse medo é constante, admito. A maioria dos voluntários é mais próxima de nós, então confiamos e sabemos que mandam bem. Mas e o restante do pessoal? Imagina se não têm didática! E se falarem algo errado durante a aula? Enfim, problemas clássicos de se trabalhar com quem não conhecemos.  

# Realidade

E a realidade veio e bateu na cara. Bateu forte. As primeiras aulas começaram nesta semana, mas eu já posso dizer... está sendo um sucesso!  

Os estudantes são extremamente engajados, sempre tirando dúvidas, sugestões, pedindo pra repetir, etc. garantindo a participação na aula! Além disso os monitores têm sido super atenciosos e solícitos, é difícil achar um horário em que não tenha nenhum disponível pra ajudar as crianças. Sinceramente todo mundo está de parabéns até agora.  

Alguns problemas aparecem, obviamente, mas nenhum que não tenhamos conseguido resolver. Eu sou uma das pessoas mais _exigentes_ (as más línguas diriam _chata_...) da organização, então sempre anoto o que estamos fazendo de errado pra que possamos melhorar no dia seguinte. Como o curso é intensivo, temos sempre que agir depressa.  

# Conclusão

Este é, sem dúvida alguma, um dos melhores e mais gratificantes projetos de que eu já participei na faculdade e na vida. Como é bom ver as coisas dando certo e aquele monte de jovens em vulnerabilidade gostando de programar. Espero de verdade que consigamos ajudá-los a traçar os primeiros passos pra que depois corram uma maratona inteira.  
